/* Trial
 * Kinect SDK 1.6 and openFrameworks 0073
 *
 * (c) 2013 Tuwuh Sarwoprasojo
 * All rights reserved.
 */

#pragma once

#include "ofMain.h"
#include <Windows.h>
#include <Ole2.h>
#include <NuiApi.h>

#define KINECT_WIDTH   (640)
#define KINECT_HEIGHT  (480)
#define KINECT_TILT    (-26)

class testApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed  (int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		bool initKinect();

		INuiSensor* kinect;  
		HANDLE colorStream;
		HANDLE depthStream;
		BYTE colorData[KINECT_WIDTH * KINECT_HEIGHT * 4];
		USHORT depthRaw[KINECT_WIDTH * KINECT_HEIGHT];
		USHORT depthMillimeters[KINECT_WIDTH * KINECT_HEIGHT];
		USHORT alignedDepthMillimeters[KINECT_WIDTH * KINECT_HEIGHT];
		BYTE depthData[KINECT_WIDTH * KINECT_HEIGHT * 4];
		LONG colorCoordinates[KINECT_WIDTH * KINECT_HEIGHT * 2];
		BYTE alignedDepthData[KINECT_WIDTH * KINECT_HEIGHT * 4];
		BYTE alignedColorData[KINECT_WIDTH * KINECT_HEIGHT * 4];

		ofTrueTypeFont dejavu;

		ofTexture colorTexture;
		ofTexture alignedDepthTexture;
		ofTexture alignedColorTexture;
		ofTexture depthTexture;
};
